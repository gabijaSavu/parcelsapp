package com.devbridge.postbridge.parcelsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParcelsApp {

    public static void main(String[] args) {

      SpringApplication.run(ParcelsApp.class, args);

    }
}
